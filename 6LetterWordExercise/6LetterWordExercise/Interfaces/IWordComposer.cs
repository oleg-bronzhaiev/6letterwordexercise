﻿using SixLetterWordExercise.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SixLetterWordExercise.Interfaces
{
	/// <summary>
	/// Model Service Interface
	/// </summary>
	interface IWordComposer
	{
		IReadOnlyList<CompoundWord> Compose(string[] lettersItemList, int compoundWordLength);
	}
}
