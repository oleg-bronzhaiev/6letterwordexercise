﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SixLetterWordExercise.Interfaces
{
	/// <summary>
	/// App Service Interface
	/// </summary>
	interface IWordExerciseService
	{
		Task<string[]> DoExerciseAsync();
	}
}
