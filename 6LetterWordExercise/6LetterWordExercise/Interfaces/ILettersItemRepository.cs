﻿using System.Threading.Tasks;

namespace SixLetterWordExercise.Interfaces
{
	/// <summary>
	/// Infrastructure Service Interface
	/// </summary>
	interface ILettersItemRepository
	{
		Task<string[]> ListAsync(int maxChars);
	}
}
