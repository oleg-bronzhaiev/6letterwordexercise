﻿using SixLetterWordExercise.Entities;
using SixLetterWordExercise.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SixLetterWordExercise.Services
{
	class WordComposer : IWordComposer
	{
		public IReadOnlyList<CompoundWord> Compose(string[] lettersItems, int compoundWordLength)
		{
			if(lettersItems.Length == 0)
			{
				return new List<CompoundWord>(0).AsReadOnly();
			}

			var itemList = new List<string>(lettersItems);
			itemList.Sort((x, y) => x.Length.CompareTo(y.Length));
			// don't remove duplicates on purpose. E.g. two identical entries "foo" may compose "foofoo" word

			var sortedItems = itemList.ToArray();

			// save "compoundWordDict" to look up the matches
			var compoundWordDict = sortedItems.Where(x => x.Length == compoundWordLength).Distinct().ToDictionary(x => x);

			var compoundWordList = new List<CompoundWord>();
			for (int i = 0; i < sortedItems.Length; i++)
			{
				var firstItem = sortedItems[i];
				if (firstItem.Length >= compoundWordLength)
				{
					break;
				}

				compoundWordList.AddRange(
					GetCompoundWordList(sortedItems, new int[] { i }, compoundWordLength, compoundWordDict));
			}
			return compoundWordList.AsReadOnly();
		}

		private List<CompoundWord> GetCompoundWordList(string[] sortedItems, int[] itemIndexList, int compoundWordLength, IDictionary<string, string> compoundWordDict)
		{
			var list = new List<CompoundWord>();

			for (int j = 0; j < sortedItems.Length; j++)
			{
				if (itemIndexList.Contains(j))
				{
					continue;
				}

				var currentItemIndexList = itemIndexList.Concat(new int[] { j }).ToArray();
				var itemsLength = currentItemIndexList.Select(x => sortedItems[x].Length).Sum();

				if (itemsLength > compoundWordLength)
				{
					break;
				}

				if (itemsLength == compoundWordLength)
				{
					var wordParts = currentItemIndexList.Select(x => sortedItems[x]);
					var word = string.Concat(wordParts);
					if (!compoundWordDict.ContainsKey(word))
					{
						continue;
					}

					list.Add(new CompoundWord(word, wordParts.ToArray()));
				}

				list.AddRange(
					GetCompoundWordList(sortedItems, currentItemIndexList, compoundWordLength, compoundWordDict));
			}

			return list;
		}
	}
}
