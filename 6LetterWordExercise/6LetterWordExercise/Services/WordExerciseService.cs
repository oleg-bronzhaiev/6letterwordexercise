﻿using SixLetterWordExercise.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace SixLetterWordExercise.Services
{
	class WordExerciseService : IWordExerciseService
	{
		private const int CONFIG_COMPOUND_WORD_LENGTH = 6;  // should be part of appsettings.json
		private readonly ILettersItemRepository _lettersItemRepository;
		private readonly IWordComposer _sixLetterWordComposer;

		public WordExerciseService(
			ILettersItemRepository lettersItemRepository,
			IWordComposer sixLetterWordComposer)
		{
			_lettersItemRepository = lettersItemRepository;
			_sixLetterWordComposer = sixLetterWordComposer;
		}

		public async Task<string[]> DoExerciseAsync()
		{
			var lettersItemList = await _lettersItemRepository.ListAsync(CONFIG_COMPOUND_WORD_LENGTH);

			var sixLetterWordList = _sixLetterWordComposer.Compose(lettersItemList, CONFIG_COMPOUND_WORD_LENGTH);

			return sixLetterWordList.Select(x => x.ToString()).ToArray();
		}
	}
}
