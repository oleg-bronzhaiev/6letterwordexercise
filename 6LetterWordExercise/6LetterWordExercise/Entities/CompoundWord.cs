﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SixLetterWordExercise.Entities
{
	/// <summary>
	/// Entity, which presents a result of computation in the exercise
	/// 
	/// A value object in DDD
	/// </summary>
	class CompoundWord
	{
		public string Word { get; private set; }

		public string[] WordParts { get; private set; }

		public CompoundWord(string sixLetterWordText, string[] wordParts)
		{
			Word = sixLetterWordText;
			WordParts = wordParts;
		}

		public override string ToString()
		{
			return $"{string.Join("+", WordParts)}={Word}";
		}
	}
}
