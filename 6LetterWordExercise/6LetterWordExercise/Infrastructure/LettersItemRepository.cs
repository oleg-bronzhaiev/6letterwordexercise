﻿using SixLetterWordExercise.Entities;
using SixLetterWordExercise.Interfaces;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace SixLetterWordExercise.Infrastructure
{
	class LettersItemRepository : ILettersItemRepository
	{
		private string PATH = @"input.txt";  // should be part of appsettings.json

		public async Task<string[]> ListAsync(int maxChars)
		{
			return await File.ReadAllLinesAsync(PATH, Encoding.UTF8);
		}
	}
}
