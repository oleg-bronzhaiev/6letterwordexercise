﻿using Microsoft.Extensions.DependencyInjection;
using SixLetterWordExercise.Infrastructure;
using SixLetterWordExercise.Interfaces;
using SixLetterWordExercise.Services;
using System;

namespace SixLetterWordExercise
{
	class Program
	{
		static void Main(string[] args)
		{
			var serviceCollection = new ServiceCollection()
				.AddTransient<IWordExerciseService, WordExerciseService>()
				.AddTransient<ILettersItemRepository, LettersItemRepository>()
				.AddTransient<IWordComposer, WordComposer>();

			var serviceProvider = serviceCollection.BuildServiceProvider();

			var exerciseService = serviceProvider.GetService<IWordExerciseService>();

			WriteResultToConsole(exerciseService);

			Console.ReadKey();
		}

		static async void WriteResultToConsole(IWordExerciseService exerciseService)
		{
			var resultList = await exerciseService.DoExerciseAsync();

			foreach (var line in resultList)
			{
				Console.WriteLine(line);
			}
		}
	}
}
